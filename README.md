# OpenML dataset: covid-19_sentiments-India200320---310520

https://www.openml.org/d/43697

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

About our Dataset
The journey of the collection of this Covid-19 India dataset begin with a competition where we have to do sentiment analysis of tweets. The data was collected from https://ieee-dataport.org/open-access/coronavirus-covid-19-tweets-dataset . This site gave us the tweet Id of relevant tweets and to extract the tweets text and other information, we used Hydrator app.
About features of dataset
There are total 5 columns.
Column 1: 'Text ID'
    It contains unique ID for each tweet.
Column 2: 'Text'
    It is the tweet text of that particular tweet ID.
Column 3: 'Date'
    The date on which the tweet was tweeted.
Column 4: 'Location'
    The place from where the tweet was tweeted.
Column 5: 'Sentiments'
    The sentiment value of that tweet, whether it is positive, negative or neutral.
     If sentiment score is greater then 0 then sentiment is positive.
     If sentiment score is equal to 0 then sentiment is neutral.
     If sentiment score is less then 0 then sentiment is negative.
Acknowledgements
We wouldn't be here without the help of others. We would like to acknowledge https://ieee-dataport.org/open-access/coronavirus-covid-19-tweets-dataset for providing the tweet Id's. We would also like to acknowledge Hydrator app for fectching tweets.
Inspiration
Actually we got the inspiration from the competition where we were given the task to categorize the sentiment values of COVID - 19 India tweets.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43697) of an [OpenML dataset](https://www.openml.org/d/43697). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43697/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43697/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43697/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

